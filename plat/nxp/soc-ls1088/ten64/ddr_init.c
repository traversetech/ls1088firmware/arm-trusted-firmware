/*
 * Copyright 2018 NXP
 * Copyright 2019 Traverse Technologies Australia
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Author: Mathew McBride <matt@traverse.com.au>
 * Based on ls1088ardb version by York Sun <york.sun@nxp.com>
 */

#include <platform_def.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <debug.h>
#include <errno.h>
#include <string.h>
#include <io.h>
#include <ddr.h>
#include <utils.h>
#include <utils_def.h>
#include <errata.h>

#ifdef CONFIG_STATIC_DDR
#error No static value defined
#endif

#define CLK_ADJ_5_8 0xA
#define CLK_ADJ_9_16 0x9
#define CLK_ADJ_11_6 0xB
#define WRLVL_5_8 0x5
#define WRLVL_7_8 0x7
#define WRLVL_1 0x8

/* Ref card A2 */
static const struct rc_timing rcA_timing[] = {
	{2134, CLK_ADJ_5_8, WRLVL_1},
	{}
};

/* Ref card B1 */
static const struct rc_timing rcB_timing[] = {
	{2134, CLK_ADJ_9_16, WRLVL_5_8},
	{}
};

static const struct rc_timing rcC_timing[] = {
	{2134, CLK_ADJ_11_6, WRLVL_1},
	{}
};

/* Ref card D1 */
static const struct rc_timing rcD_timing[] = {
	{2134, CLK_ADJ_5_8, WRLVL_7_8},
	{}
};

/* Ref card E1 */
static const struct rc_timing rcE_timing[] = {
	{2134, CLK_ADJ_9_16, WRLVL_5_8},
	{}
};

static const struct rc_timing rcG_timing[] = {
	{2134, CLK_ADJ_9_16, WRLVL_5_8},
	{}
};

/* For those playing at home..
 * rc = JEDEC Reference/Raw Card identifier
 * rc_timing = Clock adjust and base write level
 * add1 = first set of write level adjustments for each lane
 * add2 = second set
 * These settings are proportional to the relative lengths of
 * CLK-to-DQS on the DIMM (for each lane).
 */
static const struct board_timing udimm[] = {
	{0x00, rcA_timing, 0x00010204, 0x04050405}, /* A, Crucial CT4G4SFS8266.C8FE */
	{0x01, rcB_timing, 0x02ff0507, 0x0e0a0b0f}, /* B, Crucial CT16G4SFD824A.C16FDD1 */
	{0x02, rcC_timing, 0x00010103, 0x03040301}, /* C, Kingston KVR26S19S6/4 */
	{0x03, rcD_timing, 0x00010204, 0x04050502}, /* D, Innodisk M4D0-8GS1PCRG */
	{0x04, rcE_timing, 0x01FF0406, 0x0A070807},	/* E, Samsung M471A4G43MB1-CTD */
	{0x06, rcG_timing, 0x01FF0306, 0x0A070805}, /* G, Micron MTA18ASF2G72HZ-16GB */
};

int ddr_board_options(struct ddr_info *priv)
{
	int ret;
	struct memctl_opt *popts = &priv->opt;

	if (popts->rdimm) {
		debug("RDIMM parameters not set.\n");
		return -EINVAL;
	}

	ret = cal_board_params(priv, udimm, ARRAY_SIZE(udimm));
	if (ret)
		return ret;

	popts->addr_hash = 1;
	popts->cpo_sample = 0x7b;
	popts->ddr_cdr1 = DDR_CDR1_DHC_EN	|
			  DDR_CDR1_ODT(DDR_CDR_ODT_60ohm);
	popts->ddr_cdr2 = DDR_CDR2_ODT(DDR_CDR_ODT_60ohm)	|
			  DDR_CDR2_VREF_TRAIN_EN		|
			  DDR_CDR2_VREF_RANGE_2;

	return 0;
}

long long _init_ddr(void)
{
	int spd_addr[] = { NXP_SPD_EEPROM0 };
	struct ddr_info info;
	struct sysinfo sys;
	long long dram_size;
	uint32_t *gpio_addr = (uint32_t *)NXP_GPIO1_ADDR;

	/* We need to drive ASLEEP (GPIO1_28) as an output
	 * soon after boot, otherwise some boards may signal
	 * a poweroff due to the MOSFET activating accidentally
	 */
	*gpio_addr |= (1<<3); /* Set GPIO1_28 as an output */

	zeromem(&sys, sizeof(sys));
	get_clocks(&sys);
	debug("platform clock %lu\n", sys.freq_platform);
	debug("DDR PLL %lu\n", sys.freq_ddr_pll0);

	zeromem(&info, sizeof(struct ddr_info));
	info.num_ctlrs = NUM_OF_DDRC;
	info.dimm_on_ctlr = DDRC_NUM_DIMM;
	info.clk = get_ddr_freq(&sys, 0);
	info.spd_addr = spd_addr;
	info.ddr[0] = (void *)NXP_DDR_ADDR;

	dram_size = dram_init(&info);

	if (dram_size < 0)
		ERROR("DDR init failed.\n");

	erratum_a008850_post();

	return dram_size;
}
